package test.unit;

import com.usac.greenpulse.air.dto.aq.AirQualityResponseDto;
import com.usac.greenpulse.air.dto.aqicn.AqicnResponseDto;
import com.usac.greenpulse.air.dto.search.SearchDto;
import com.usac.greenpulse.air.model.entity.search.Search;
import com.usac.greenpulse.air.repository.search.SearchRepository;
import com.usac.greenpulse.air.service.search.SearchServiceImpl;
import com.usac.greenpulse.air.service.timeprovider.TimeProvicerService;
import org.junit.jupiter.api.BeforeEach;
import com.usac.greenpulse.air.security.SecurityContext;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;

import static org.assertj.core.api.Assertions.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class SearchServiceImplTest {
    private SearchRepository searchRepository = mock(SearchRepository.class);
    private SecurityContext securityContext = mock(SecurityContext.class);
    private SearchServiceImpl searchService;
    private TimeProvicerService timeProvicerService = mock(TimeProvicerService.class);

    private AirQualityResponseDto AIRQUALITYRESPONSEDTO;
    private Long USER_ID = 1L;
    private final String STATUS = "OK";
    private final Integer AQI = 13;
    private final Integer IDX = 12515;
    private final BigDecimal LATITUDE = BigDecimal.valueOf(14.6069387);
    private final BigDecimal LONGITUD = BigDecimal.valueOf(-90.5169696);
    private final String NAME = "US Embassy, Guatemala City, Guatemala";
    private final String URL = "https://aqicn.org/city/guatemala/guatemala-city/us-embassy";
    private final String LOCATION = "";
    private final LocalDateTime TIME = LocalDateTime.parse("2024-06-23 23:00:00", DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
    private AqicnResponseDto aqicnResponseDto;
    private AirQualityResponseDto airQualityResponseDto;


    @BeforeEach
    void setup(){
        searchService = new SearchServiceImpl(searchRepository,securityContext,timeProvicerService);

        AqicnResponseDto.City city = new AqicnResponseDto.City();
        city.setGeo(Arrays.asList(LATITUDE,LONGITUD));
        city.setName(NAME);
        city.setUrl(URL);
        city.setLocation(LOCATION);

        AqicnResponseDto.Data data = new AqicnResponseDto.Data();
        data.setAqi(AQI+"");
        data.setIdx(IDX);
        data.setCity(city);

        aqicnResponseDto = new AqicnResponseDto();
        aqicnResponseDto.setStatus(STATUS);
        aqicnResponseDto.setData(data);

        airQualityResponseDto = new AirQualityResponseDto(aqicnResponseDto,timeProvicerService.now());
    }
    @Test
    void save(){
        when(securityContext.getUserId()).thenReturn(USER_ID);

        Search search = new Search();
        search.setUserId(USER_ID);
        search.setAqi(airQualityResponseDto.getAqi());
        search.setLatitude(airQualityResponseDto.getLatitude());
        search.setLongitude(LONGITUD);
        search.setEntryDate(TIME);

        when(searchRepository.save(search)).thenReturn(search);
        when(timeProvicerService.now()).thenReturn(TIME);

        SearchDto expected = new SearchDto(search);
        SearchDto actually = searchService.create(airQualityResponseDto);

        assertThat(actually).isEqualToComparingFieldByFieldRecursively(expected);
    }
}
