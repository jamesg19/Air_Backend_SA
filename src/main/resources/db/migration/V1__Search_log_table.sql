CREATE TABLE IF NOT EXISTS search(
                                     id BIGINT,
                                     user_id BIGINT NOT NULL,
                                     position_x VARCHAR(50) NOT NULL,
                                     position_y VARCHAR (50) NOT NULL,
                                     no_aqi INT NOT NULL,
                                     CONSTRAINT search_pk PRIMARY KEY (id),
                                     CONSTRAINT person_search_pk FOREIGN KEY (user_id) REFERENCES _user(user_id)
);

CREATE SEQUENCE SEQ_SEARCH_LOG START WITH 10000 INCREMENT BY 1;