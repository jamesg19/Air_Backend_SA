package com.usac.greenpulse.air.repository.search;

import com.usac.greenpulse.air.model.entity.search.Search;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SearchRepository extends CrudRepository<Search, Long> {
}
