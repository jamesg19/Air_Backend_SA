package com.usac.greenpulse.air.controller.aq;

import com.usac.greenpulse.air.dto.aq.AirQualityRequestDto;
import com.usac.greenpulse.air.dto.aq.AirQualityResponseDto;
import com.usac.greenpulse.air.dto.aq.AqiCountryRankingResponse;
import com.usac.greenpulse.air.dto.aqicn.AqicnResponseDto;
import com.usac.greenpulse.air.service.aqicn.AqicnService;
import com.usac.greenpulse.air.service.search.SearchService;
import com.usac.greenpulse.air.service.timeprovider.TimeProvicerService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.*;

@RestController
@RequestMapping("/v1/aq")
@RequiredArgsConstructor
public class AirQualityController {

    @Value("${aqicn.token}")
    private String token;
    private final AqicnService aqicnService;
    private final SearchService searchService;
    private final TimeProvicerService timeProvicerService;
    private final List<String> countries = Arrays.asList(
            "Afghanistan", "Albania", "Algeria", "Andorra", "Angola", "Antigua and Barbuda", "Argentina", "Armenia", "Australia",
            "Austria", "Azerbaijan", "Bahamas", "Bahrain", "Bangladesh", "Barbados", "Belarus", "Belgium", "Belize", "Benin",
            "Bhutan", "Bolivia", "Bosnia and Herzegovina", "Botswana", "Brazil", "Brunei", "Bulgaria", "Burkina Faso", "Burundi",
            "Cabo Verde", "Cambodia", "Cameroon", "Canada", "Central African Republic", "Chad", "Chile", "China", "Colombia",
            "Comoros", "Congo", "Costa Rica", "Croatia", "Cuba", "Cyprus", "Czech", "Democratic Republic of the Congo",
            "Denmark", "Djibouti", "Dominica", "Dominican Republic", "Ecuador", "Egypt", "El Salvador", "Equatorial Guinea", "Eritrea",
            "Estonia", "Eswatini", "Ethiopia", "Fiji", "Finland", "France", "Gabon", "Gambia", "Georgia", "Germany",
            "Ghana", "Greece", "Grenada", "Guatemala", "Guinea", "Guinea-Bissau", "Guyana", "Haiti", "Holy See", "Honduras", "Hungary",
            "Iceland", "India", "Indonesia", "Iran", "Iraq", "Ireland", "Israel", "Italy", "Jamaica", "Japan", "Jordan", "Kazakhstan",
            "Kenya", "Kiribati", "Kuwait", "Kyrgyzstan", "Laos", "Latvia", "Lebanon", "Lesotho", "Liberia", "Libya", "Liechtenstein",
            "Lithuania", "Luxembourg", "Madagascar", "Malawi", "Malaysia", "Maldives", "Mali", "Malta", "Marshall Islands", "Mauritania",
            "Mauritius", "Mexico", "Micronesia", "Moldova", "Monaco", "Mongolia", "Montenegro", "Morocco", "Mozambique", "Myanmar",
            "Namibia", "Nauru", "Nepal", "Netherlands", "New Zealand", "Nicaragua", "Niger", "Nigeria", "North Korea", "North Macedonia",
            "Norway", "Oman", "Pakistan", "Palau", "Palestine State", "Panama", "Papua New Guinea", "Paraguay", "Peru", "Philippines", "Poland",
            "Portugal", "Qatar", "Romania", "Russia", "Rwanda", "Saint Kitts and Nevis", "Saint Lucia", "Saint Vincent and the Grenadines", "Samoa",
            "San Marino", "Sao Tome and Principe", "Saudi Arabia", "Senegal", "Serbia", "Seychelles", "Sierra Leone", "Singapore", "Slovakia",
            "Slovenia", "Solomon Islands", "Somalia", "South Africa", "South Korea", "South Sudan", "Spain", "Sri Lanka", "Sudan", "Suriname",
            "Sweden", "Switzerland", "Syria", "Tajikistan", "Tanzania", "Thailand", "Timor-Leste", "Togo", "Tonga", "Trinidad and Tobago",
            "Tunisia", "Turkey", "Turkmenistan", "Tuvalu", "Uganda", "Ukraine", "United Arab Emirates", "United Kingdom", "Estados Unidos",
            "Uruguay", "Uzbekistan", "Vanuatu", "Venezuela", "Vietnam", "Yemen", "Zambia", "Zimbabwe"
    );

    @GetMapping("here")
    public ResponseEntity<AirQualityResponseDto> here() {
        var response = aqicnService.myAQ(token);
        if (response.getBody() != null) {
            AirQualityResponseDto responseDto = new AirQualityResponseDto(response.getBody(),timeProvicerService.now());
            this.searchService.create(responseDto);
            return ResponseEntity.ok(responseDto);
        } else {
            return ResponseEntity.status(response.getStatusCode()).build();
        }
    }

    @GetMapping("city")
    public ResponseEntity<AirQualityResponseDto> city(
            @RequestParam("cityId") String cityId
    ) {
        try {
            var response = aqicnService.getCity(cityId, token);
            if (response.getBody() != null) {
                AirQualityResponseDto responseDto = new AirQualityResponseDto(response.getBody(),timeProvicerService.now());
                this.searchService.create(responseDto);
                return ResponseEntity.ok(responseDto);
            } else {
                return ResponseEntity.status(response.getStatusCode()).build();
            }
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }
    }

    @GetMapping("specific")
    public ResponseEntity<AirQualityResponseDto> specific(
            @RequestParam("latitude") BigDecimal latitude,
            @RequestParam("longitude") BigDecimal longitude
    ) {
        var response = aqicnService.specificAQ(latitude, longitude, token);
        if (response.getBody() != null) {
            AirQualityResponseDto responseDto = new AirQualityResponseDto(response.getBody(),timeProvicerService.now());
            this.searchService.create(responseDto);
            return ResponseEntity.ok(responseDto);
        } else {
            return ResponseEntity.status(response.getStatusCode()).build();
        }
    }

    @GetMapping("ranking")
    public ResponseEntity<List<AqiCountryRankingResponse>> ranking() {
        List<AqiCountryRankingResponse> list_ranking = new ArrayList<>();

        for (int i = 0; i < countries.size(); i++) {
            String country = countries.get(i);
            try {
                var response = aqicnService.getCity(country, token);
                list_ranking.add(new AqiCountryRankingResponse(response.getBody(),country));
            } catch (Exception e) {
                //
            }
        }
        Comparator<AqiCountryRankingResponse> aqiComparator = new Comparator<AqiCountryRankingResponse>() {
            @Override
            public int compare(AqiCountryRankingResponse a1, AqiCountryRankingResponse a2) {
                return Integer.compare(a2.getAqi(), a1.getAqi());
            }
        };

        Collections.sort(list_ranking, aqiComparator);
        return ResponseEntity.ok(list_ranking);
    }
}
