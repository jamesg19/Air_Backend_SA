package com.usac.greenpulse.air.model.entity.token;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;

/**
 * The persistence class for the token_credential database table.
 * @author willyrex
 */

@Entity
@Table(name = "token_credential")
@Getter
@Setter
@NoArgsConstructor
public class TokenCredential {

    @Id
    @Column(name = "token_credential_id")
    @SequenceGenerator(name = "tokenCreIdGen", sequenceName = "SEQ_TOKEN_CREDENTIAL", allocationSize = 1, initialValue = 10000)
    @GeneratedValue(generator = "tokenCreIdGen", strategy = GenerationType.SEQUENCE)
    private Long tokenCredentialId;

    @Column(name = "user_email")
    private String userEmail;

    @Column(name = "token")
    private String token;

    @Column(name = "entry_date")
    private LocalDateTime entryDate;

    @Column(name = "expiry_date")
    private LocalDateTime expiryDate;

}
