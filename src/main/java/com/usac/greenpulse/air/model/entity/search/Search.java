package com.usac.greenpulse.air.model.entity.search;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * The persistence class for the search database table.
 * @author willyrex
 */

@Entity
@Table(name = "search")
@Getter
@Setter
@NoArgsConstructor
public class Search {

    @Id
    @Column(name = "search_id")
    @SequenceGenerator(name = "searchIdGen", sequenceName = "SEQ_SEARCH_LOG", allocationSize = 1, initialValue = 10000)
    @GeneratedValue(generator = "searchIdGen", strategy = GenerationType.SEQUENCE)
    private Long searchId;

    @Column(name = "user_id")
    private Long userId;

    @Column(name = "aqi")
    private Integer aqi;

    @Column(name = "latitude")
    private BigDecimal latitude;

    @Column(name = "longitude")
    private BigDecimal longitude;

    @Column(name = "entry_date")
    private LocalDateTime entryDate;

}
