package com.usac.greenpulse.air.configuration;

import com.usac.greenpulse.air.service.aqicn.AqicnService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestClient;
import org.springframework.web.client.support.RestClientAdapter;
import org.springframework.web.service.invoker.HttpServiceProxyFactory;

@Configuration
public class AqicnWebConfig {

    @Value("${aqicn.url}")
    private String url;

    @Bean
    AqicnService aqicnService() {
        RestClient restClient = RestClient.builder().baseUrl(url).build();
        RestClientAdapter adapter = RestClientAdapter.create(restClient);
        HttpServiceProxyFactory factory = HttpServiceProxyFactory.builderFor(adapter).build();
        return factory.createClient(AqicnService.class);
    }

}
