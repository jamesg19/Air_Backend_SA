package com.usac.greenpulse.air.security;

import org.springframework.security.core.userdetails.UserDetails;

public interface JwtService {

    String extractUsername(String token);

    Boolean isTokenValid(String token, UserDetails userDetails);

}
