package com.usac.greenpulse.air.security;

import com.usac.greenpulse.air.model.entity.token.TokenCredential;
import com.usac.greenpulse.air.service.token.TokenCredentialService;
import com.usac.greenpulse.air.util.KeyLoader;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.Optional;
import java.util.function.Function;

@Service
@RequiredArgsConstructor
@Setter
public class JwtServiceImpl implements JwtService{

    @Value("${key.location.public}")
    private String publicKeyLocation;

    private final KeyLoader keyLoader;

    private final TokenCredentialService tokenCredentialService;

    @Override
    public String extractUsername(String token) {
        try {
            return this.extractClaim(token, Claims::getSubject);
        }catch (Exception e){
            return null;
        }
    }

    @Override
    public Boolean isTokenValid(String token, UserDetails userDetails) {
        try {
            return this.extractUsername(token).equals(userDetails.getUsername()) && !this.hasTokenExpired(token);
        } catch (Exception e){
            return null;
        }
    }

    private <T> T extractClaim(String token, Function<Claims, T> resolvers) throws IOException {
        Claims claims = this.extractAllClaims(token);
        return resolvers.apply(claims);
    }

    private Claims extractAllClaims(String token) throws IOException {
        return Jwts.parser()
                .verifyWith(this.keyLoader.loadPublicKey(this.publicKeyLocation))
                .build()
                .parseSignedClaims(token)
                .getPayload()
        ;
    }

    private boolean hasTokenExpired(String token) {
        Optional<TokenCredential> optional = this.tokenCredentialService.findByToken(token);
        if (optional.isEmpty()) return true;
        boolean expired = optional.get().getExpiryDate().isBefore(LocalDateTime.now());
        if (!expired) this.tokenCredentialService.incrementExpiryDate(optional.get());
        return expired;
    }

}
