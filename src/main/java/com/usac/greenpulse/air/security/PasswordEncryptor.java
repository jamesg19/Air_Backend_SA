package com.usac.greenpulse.air.security;

import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.crypto.password.Pbkdf2PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class PasswordEncryptor implements PasswordEncoder {

    private final Pbkdf2PasswordEncoder passwordEncoder;

    public PasswordEncryptor() {
        this.passwordEncoder = new Pbkdf2PasswordEncoder(
                "mySecretKe",
                64,
                2777,
                Pbkdf2PasswordEncoder.SecretKeyFactoryAlgorithm.PBKDF2WithHmacSHA512
        );
    }

    @Override
    public String encode(CharSequence rawPassword) {
        return this.passwordEncoder.encode(rawPassword);
    }

    @Override
    public boolean matches(CharSequence rawPassword, String encodedPassword) {
        return this.passwordEncoder.matches(rawPassword, encodedPassword);
    }

}
