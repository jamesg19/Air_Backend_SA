package com.usac.greenpulse.air.exception;

import lombok.Getter;
import org.springframework.http.HttpStatus;

@Getter
public class GeneralException extends Exception {

    private final HttpStatus status;

    public GeneralException(HttpStatus status) {
        this.status = status;
    }

    public GeneralException(String message, HttpStatus status) {
        super(message);
        this.status = status;
    }

}
