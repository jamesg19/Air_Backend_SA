package com.usac.greenpulse.air.service.search;

import com.usac.greenpulse.air.dto.aq.AirQualityResponseDto;
import com.usac.greenpulse.air.dto.search.SearchDto;
import com.usac.greenpulse.air.model.entity.search.Search;
import com.usac.greenpulse.air.repository.search.SearchRepository;
import com.usac.greenpulse.air.security.SecurityContext;
import com.usac.greenpulse.air.service.timeprovider.TimeProvicerService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

@Service
@RequiredArgsConstructor
public class SearchServiceImpl implements SearchService {

    private final SearchRepository searchRepository;
    private final SecurityContext securityContext;
    private final TimeProvicerService timeProvicerService;

    @Override
    public SearchDto create(AirQualityResponseDto response) {
        Search search = new Search();
        search.setUserId(this.securityContext.getUserId());
        search.setAqi(response.getAqi());
        search.setLatitude(response.getLatitude());
        search.setLongitude(response.getLongitude());
        search.setEntryDate(timeProvicerService.now());
        this.searchRepository.save(search);
        return new SearchDto(search);
    }

}
