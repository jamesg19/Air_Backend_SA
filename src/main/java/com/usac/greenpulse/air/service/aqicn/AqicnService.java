package com.usac.greenpulse.air.service.aqicn;

import com.usac.greenpulse.air.dto.aqicn.AqicnResponseDto;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.service.annotation.GetExchange;
import org.springframework.web.service.annotation.HttpExchange;

import java.math.BigDecimal;

@HttpExchange(url = "/feed")
public interface AqicnService {

    @GetExchange(url = "/here")
    ResponseEntity<AqicnResponseDto> myAQ(@RequestParam(name = "token") String token);

    @GetExchange(url = "/geo:{lat};{long}")
    ResponseEntity<AqicnResponseDto> specificAQ(
            @PathVariable(name = "lat") BigDecimal latitude,
            @PathVariable(name = "long") BigDecimal longitude,
            @RequestParam(name = "token") String token
    );

    @GetExchange(url = "/{city}/")
    ResponseEntity<AqicnResponseDto> getCity(
            @PathVariable(name = "city") String city,
            @RequestParam(name = "token") String token
    );

}
