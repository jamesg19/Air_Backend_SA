package com.usac.greenpulse.air.service.timeprovider;

import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

@Service
public class TimeProviderImpl implements TimeProvicerService{
    @Override
    public LocalDateTime now() {
        return LocalDateTime.now();
    }
}
