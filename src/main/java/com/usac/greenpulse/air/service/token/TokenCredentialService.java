package com.usac.greenpulse.air.service.token;



import com.usac.greenpulse.air.model.entity.token.TokenCredential;

import java.time.LocalDateTime;
import java.util.Optional;

public interface TokenCredentialService {

    void create(String email, String token);

    void incrementExpiryDate(TokenCredential tokenCredential);

    void invalidateTokens(String email, LocalDateTime expiryDate);

    Optional<TokenCredential> findByToken(String token);

}
