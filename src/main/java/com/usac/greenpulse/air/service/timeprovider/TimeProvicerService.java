package com.usac.greenpulse.air.service.timeprovider;

import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

public interface TimeProvicerService {
    LocalDateTime now();
}
