package com.usac.greenpulse.air.service.search;

import com.usac.greenpulse.air.dto.aq.AirQualityResponseDto;
import com.usac.greenpulse.air.dto.search.SearchDto;

public interface SearchService {

    SearchDto create(AirQualityResponseDto response);

}
