package com.usac.greenpulse.air.dto.aq;

import com.usac.greenpulse.air.dto.aqicn.AqicnResponseDto;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Getter
@Setter
public class AqiCountryRankingResponse {
    private String country;
    private Integer aqi;
    private BigDecimal latitude;
    private BigDecimal longitude;

    public AqiCountryRankingResponse(AqicnResponseDto aqicnResponseDto, String country) {
        this.aqi = aqicnResponseDto.getData().getAqi().equalsIgnoreCase("-")?0:Integer.parseInt(aqicnResponseDto.getData().getAqi());
        this.country = country;
        this.latitude = aqicnResponseDto.getData().getCity().getGeo().get(0);
        this.longitude = aqicnResponseDto.getData().getCity().getGeo().get(1);
    }
}
