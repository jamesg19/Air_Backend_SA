package com.usac.greenpulse.air.dto.aqicn;

import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.util.List;

@Getter
@Setter
public class AqicnResponseDto {

    private String status;
    private Data data;

    @Getter
    @Setter
    public static class Data {

        private String aqi;
        private Integer idx;
        private City city;
        private String dominentpol;
        private Iaqi iaqi;
        private TimeAQI time;
    }

    @Getter
    @Setter
    public static class TimeAQI {

        private String iso;
        private String s;
        private String tz;
        private Long v;
    }
    @Getter
    @Setter
    public static class City {

        private List<BigDecimal> geo;
        private String name;
        private String url;
        private String location;
    }
    @Getter
    @Setter
    public static class Iaqi {

        private Value co;
        private Value h;
        private Value no2;
        private Value o3;
        private Value p;
        private Value pm10;
        private Value pm25;
        private Value so2;
        private Value t;
        private Value w;

    }
    @Getter
    @Setter
    public static class Value {

        private BigDecimal v;

    }

}
