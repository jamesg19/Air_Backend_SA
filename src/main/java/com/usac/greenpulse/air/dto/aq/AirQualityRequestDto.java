package com.usac.greenpulse.air.dto.aq;

import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;

@Getter
@Setter
public class AirQualityRequestDto {

    @NotNull
    private BigDecimal latitude;
    @NotNull
    private BigDecimal longitude;

}
