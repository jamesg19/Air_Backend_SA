package com.usac.greenpulse.air.dto.search;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.usac.greenpulse.air.model.entity.search.Search;
import com.usac.greenpulse.air.util.Format;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Getter
@Setter
public class SearchDto {

    private Long userId;
    private Integer aqi;
    private BigDecimal latitude;
    private BigDecimal longitude;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = Format.DATETIME_FORMAT)
    private LocalDateTime entryDate;

    public SearchDto(Search search) {
        this.userId = search.getUserId();
        this.aqi = search.getAqi();
        this.latitude = search.getLatitude();
        this.longitude = search.getLongitude();
        this.entryDate = search.getEntryDate();
    }

}
