package com.usac.greenpulse.air.dto.aq;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.usac.greenpulse.air.dto.aqicn.AqicnResponseDto;
import com.usac.greenpulse.air.util.Format;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Getter
@Setter
public class AirQualityResponseDto {

    private String status;
    private Integer aqi;
    private Integer idx;
    private BigDecimal latitude;
    private BigDecimal longitude;
    private String name;
    private String url;
    private String location;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = Format.DATETIME_FORMAT)
    private LocalDateTime time;
    private AqicnResponseDto original;

    public AirQualityResponseDto(AqicnResponseDto aqicnResponseDto, LocalDateTime time) {
        this.status = aqicnResponseDto.getStatus();
        this.aqi = aqicnResponseDto.getData().getAqi().equalsIgnoreCase("-")?0:Integer.parseInt(aqicnResponseDto.getData().getAqi());
        this.idx = aqicnResponseDto.getData().getIdx();
        this.latitude = aqicnResponseDto.getData().getCity().getGeo().get(0);
        this.longitude = aqicnResponseDto.getData().getCity().getGeo().get(1);
        this.name = aqicnResponseDto.getData().getCity().getName();
        this.url = aqicnResponseDto.getData().getCity().getUrl();
        this.location = aqicnResponseDto.getData().getCity().getLocation();
        this.time = time;
        this.original = aqicnResponseDto;
    }

}
