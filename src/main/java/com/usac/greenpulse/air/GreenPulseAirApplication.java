package com.usac.greenpulse.air;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@SpringBootApplication
@EnableTransactionManagement
public class GreenPulseAirApplication {

	public static void main(String[] args) {
		SpringApplication.run(GreenPulseAirApplication.class, args);
	}

}
